import React from 'react'
import { useState } from 'react'
import axios from 'axios'


import ExportExcel from './pages/ExportExcel'
import ExportExcel2 from './pages/ExportExcel2'
import { ExportToExcel } from './pages/ExportToExcel'



function App() {
  // const [count, setCount] = useState(0)
  const [data, setData] = React.useState<any | null>([])
  const fileName = "myfile"; // here enter filename for your excel file

  React.useEffect(() => {
    const fetchData = () =>{
     axios.get('https://jsonplaceholder.typicode.com/posts').then(postData => {

     // reshaping the array
    //  const customHeadings = postData.data.map((item: { id: any; title: any })=>({
    //    "Article Id": item.id,
    //    "Article Title": item.title
    //  }))
    const customHeadings = [
      {id: 1, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 2, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 3, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 4, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 5, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 6, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 7, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 8, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 9, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 10, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 11, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 12, name: "a1", salary: 10000, last: "1/1/2022"},
      {id: 13, name: "a1", salary: 10000, last: "1/1/2022"},
    ]

      setData(customHeadings) 
     })
    }
    fetchData()
  }, [])
  return (
    <>
      <ExportExcel />
      <ExportExcel2 />
      <ExportToExcel apiData={data} fileName={fileName} />
    </>
  )
}

export default App
